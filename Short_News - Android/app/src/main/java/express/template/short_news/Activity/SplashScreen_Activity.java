package express.template.short_news.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import express.template.short_news.R;
import express.template.short_news.utils.SPmanager;

public class SplashScreen_Activity extends AppCompatActivity {

    private Handler handler;
    public static String themeKEY;
    private RelativeLayout relayFull;
    private TextView txtSplashTitle, txtSplashSubTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen_);

        getSupportActionBar().hide();
        statusBarColor();

        relayFull = findViewById(R.id.relayFull);
        txtSplashTitle = findViewById(R.id.txtSplashTitle);
        txtSplashSubTitle = findViewById(R.id.txtSplashSubTitle);

        themeKEY = SPmanager.getPreference(SplashScreen_Activity.this, "themeKEY");

        if (themeKEY != null) {
            if (themeKEY.equals("1")) {
                relayFull.setBackgroundResource(R.drawable.ic_spash_light);
                txtSplashTitle.setTextColor(getResources().getColor(R.color.darkDray));
                txtSplashSubTitle.setTextColor(getResources().getColor(R.color.darkDray));

            } else if (themeKEY.equals("0")) {
                relayFull.setBackgroundResource(R.drawable.ic_spash_dark);
                txtSplashTitle.setTextColor(getResources().getColor(R.color.yellow));
                txtSplashSubTitle.setTextColor(getResources().getColor(R.color.white));
            }
        } else {
            relayFull.setBackgroundResource(R.drawable.ic_spash_dark);
            txtSplashTitle.setTextColor(getResources().getColor(R.color.yellow));
            txtSplashSubTitle.setTextColor(getResources().getColor(R.color.white));

        }

//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(SplashScreen_Activity.this, Loading_Activity.class);
                startActivity(intent);

            }
        }, 2000);
    }

    private void statusBarColor() {
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.gray));
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }
}
